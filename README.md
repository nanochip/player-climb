# [[TF2] Player Climb](https://forums.alliedmods.net/showthread.php?p=2281610)

## Description:
Climb walls with melee by attacking the heck out of a frisky wall! ;)
I ripped SickleClimbWalls from VSH and added a couple cvars.
Decided to release this to the public since a lot of people want it for their FF2 server (and I use it for my prophunt server).
Edit your climbing preferences in the sm_settings menu or sm_playerclimb command.

## Video Demonstration:
[![[TF2] Nanobot - Dodgeball Bot - Player vs Bot (Demonstration) ](https://img.youtube.com/vi/I4Tgd3-aNEs/0.jpg)](https://www.youtube.com/watch?v=I4Tgd3-aNEs)

## CVARS:
Listed in your cfg/sourcemod/PlayerClimb.cfg
Note that the default settings for these cvars remedy the VSH bushwacka climb for sniper only.
* **sm_playerclimb_version** - Plugin version, no touchy!
* **sm_playerclimb_enable [0/1]** - (Default: 1) Enable the plugin? 1 = Yes, 0 = No.
* **sm_playerclimb_damageamount [#]** - (Default: 15.0) How much damage should the player take while climbing?
* **sm_playerclimb_team [0/1/2]** - (Default: 0) Restrict climbing to X team only. 0 = No restriction, 1 = BLU, 2 = RED.
* **sm_playerclimb_boss [0/1]** - (Default: 0) [VSH/FF2 ONLY] Should the boss be able to climb walls? 1 = Yes, 0 = No.
* **sm_playerclimb_class "scout,sniper,spy,heavy,soldier,demo,medic,py ro,engineer"** - (Default: "sniper") Which classes should be allowed to climb walls? Use "all" for all classes, otherwise specify the class name separated by a comma.
* **sm_playerclimb_maxclimbs [#]** - (Default: 0) The maximum amount of times the player can melee the wall (climb) while being in the air before they have to touch the ground again. 0 = Disabled, 1 = 1 Climb... 23 = 23 Climbs.
* **sm_playerclimb_cooldown [#.#]** - (Default: 0.0) Time in seconds before the player may climb the wall again, this cooldown starts when the player touches the ground after climbing.
* **sm_playerclimb_nextclimb [#.#]** - (Default: 1.56) Time in seconds in between melee climbs.

## Override:
Put this in your addons/sourcemod/configs/admin_overrides.cfg if you wish to limit player climbing to a specific flag.
* **sm_playerclimb_override** - By default, everyone has access to climbing.

## Installation:
1. Extract the zip to your addons/sourcemod/ folder.
2. Restart the server/map or type "sm plugins load playerclimb" in your server console.

## To-Do List:
I am always welcome to suggestions!
* none


## FF2 Boss Specific Climbing (Ability):
I made this plugin into a subplugin for Freak Fortress 2, check it out [here](https://forums.alliedmods.net/showthread.php?t=262572).

## Credits:
Since I ripped some code from VSH, these beautiful people deserve credit:
* **[Mecha the Slag](https://forums.alliedmods.net/member.php?u=55835)** - Made SickleClimbWalls.
* **[Eggman](https://forums.alliedmods.net/member.php?u=67367)**
* **[FlaminSarge](https://forums.alliedmods.net/member.php?u=84304)**
* **[Chdata](https://forums.alliedmods.net/member.php?u=191919)**